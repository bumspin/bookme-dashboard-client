// JavaScript Document

(function(){
	"use strict";
	
	var briefOne = $(".job-brief-one"),
		briefTwo = $(".job-brief-two"),
		modal = $("#modal"),
		//loader = $(".loader"),
		modalTitle = $(".modal-title"),
		modalBody = $(".modal-body");
	
	modal.on("hidden.bs.modal", function(){
		modalTitle.text("");
		modalBody.empty();
	});
	
	briefOne.on('click', function(){
		modal.on('shown.bs.modal', function(){
			modalBody.html("<div style='text-align: center'><img src='img/loader.gif' style='display: inline-block;'/></div>");
			setTimeout(function(){
				modalTitle.text("Horticulture & Gardening");
				modalBody.load("modal/job-brief-one.html");
			},1000);
		});	
	});
	
	briefTwo.on('click', function(){
		modal.on('shown.bs.modal', function(){
			modalBody.html("<div style='text-align: center'><img src='img/loader.gif' style='display: inline-block;'/></div>");
			setTimeout(function(){
				modalTitle.text("Graphic");
				modalBody.load("modal/job-brief-two.html");
			},1000);
		});
	});
}());